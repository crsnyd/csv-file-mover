(ns csv-file-mover.core
  (:gen-class)
  (:require [csv-file-mover.service :as service]))

(defn -main [& args]
   (service/write-moved-file (service/mover service/parts-file) service/output-csv))
