(ns csv-file-mover.service
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.string :as s]
           ))

(def target-file-path "image/");location of the files you want to go through
(def moved-to "moved/")
(def check-mime-type ".jpg")
(def parts-file "csv/Pat-image2.csv")
(def output-csv "file-result.csv")

(defn csv-reader [file]
  (with-open [in-file (io/reader file)]
   (doall (csv/read-csv in-file))))

(defn copy-file [filename]
  (io/copy (io/file (str target-file-path filename)) (io/file (str moved-to filename))))

(defn case-insenstive-check-file [filepath filename]
  (let [file-list (.list (io/as-file filepath))]
    (.contains 
      (map #(s/lower-case %) (vec file-list))
      (s/lower-case (str filename)))))

(defn check-file [filepath filename]
  (let [full-path (str filepath filename)
        quick-check (.exists (io/as-file full-path))]
    quick-check))

(defn move-file [filename]
  (let [copy (copy-file filename)
        ismoved (check-file moved-to filename)]
    (if ismoved (vector filename "true") (vector filename "problem moving file"))))

(defn file-not-found [filename]
 (vector filename "false"))

(defn file-found-with-different-name [filename] 
  (vector filename "WARNING"))

(defn go-tester [value]
  (let [filename (str (first value) check-mime-type)
       is-there (check-file target-file-path filename )]
 (if is-there (move-file filename) 
  (if (case-insenstive-check-file target-file-path filename)
    (file-found-with-different-name filename)
   (file-not-found filename)))))

(defn write-moved-file[fileobj, filename]
 (with-open [out-file (io/writer filename)]
  (csv/write-csv out-file fileobj)))

(defn mover [file]
   (into [] (map go-tester (csv-reader file))))
